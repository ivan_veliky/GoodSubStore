# Generated by Django 2.0.3 on 2018-06-28 18:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0020_remove_cartline_subtype'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartline',
            name='subtype',
            field=models.CharField(default='', max_length=10),
        ),
    ]
