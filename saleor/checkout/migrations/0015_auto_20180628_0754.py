# Generated by Django 2.0.3 on 2018-06-28 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0014_cartline_subtype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartline',
            name='quantity',
            field=models.CharField(max_length=10),
        ),
    ]
