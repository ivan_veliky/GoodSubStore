from django.conf.urls import url
from django.urls import include, path

from . import views

urlpatterns = [
    path('about-shipping/', views.about_shipping, name='about_shipping'),
    path('homepage/', views.homepage, name='homepage'),
    path('faq/', views.page_faq, name='faq'),
    url(r'^(?P<slug>[a-z0-9-_]+?)/$',
        views.page_details, name='details')]
