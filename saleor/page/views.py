import datetime

from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse

from .utils import pages_visible_to_user
from ..order.models import Order


def page_details(request, slug):
    page = get_object_or_404(
        pages_visible_to_user(user=request.user).filter(slug=slug))
    today = datetime.date.today()
    is_visible = (
        page.available_on is None or page.available_on <= today)
    return TemplateResponse(
        request, 'page/details.html', {
            'page': page, 'is_visible': is_visible})


def page_faq(request):
    return TemplateResponse(request, 'page/faq.html')

 
def homepage(request):
    return TemplateResponse(request, 'page/index.html')


def about_shipping(request):
    ctx = {'day':'0', 'placed':'0'}
    if request.user.is_authenticated:
        user = request.user
        try:
            order = Order.objects.get(user=user)
        except Order.DoesNotExist:
            order = None
        if order:
            ctx = {
                'day':order.week_day,
                'placed':order.created.date().strftime("%m/%d/%y")
            }
    return TemplateResponse(request, 'page/aboutShipping.html', ctx)
