import {setActive} from './cart';

export default $(document).ready((e) => {

var themonth = 0;
var dayLabels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
var tabVal = '2_weeks';
var weekDay = $('#week_day').attr('value');
var created = $('#created').attr('value');
var createdDate = new Date(created);
var weeks2Count = 0;
//alert(createdDate.getDate()+" "+created+" "+createdDate.getFullYear()+" "+createdDate.getMonth());

renderCal(themonth, tabVal);

$('.minusmonth').click(function(){
  themonth += -1;
  renderCal(themonth, tabVal);
});

$('.addmonth').click(function(){
  themonth += 1;
  renderCal(themonth, tabVal);
});

var header = $('#card-header-schedule');
header.find('li').each( function() {
  $(this).click((e)=> {
    e.preventDefault();
    setActive($(this).find('a'), header);
    tabVal = $(this).attr('value');
    weeks2Count=0;
    renderCal(themonth, tabVal);
  })
});

function renderCal(themonth, tab){
$('.calendar li').remove();
$('.calendar ul').append('<li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li><li>Sun</li> ');
var d = new Date(),
  currentMonth = d.getMonth()+themonth, // get this month
  days = numDays(currentMonth,d.getFullYear()), // get number of days in the month
  fDay = firstDay(currentMonth,d.getFullYear()), // find what day of the week the 1st lands on
  months = ['January','February','March','April','May','June','July','August','September','October','November','December']; // month names

$('.calendar p.monthname').text(months[currentMonth]); // add month name to calendar

for (var i=0;i<fDay;i++) { // place the first day of the month in the correct position
  $('<li class="empty">&nbsp;</li>').appendTo('.calendar ul');
}

for (var i = 1;i<=days;i++) { // write out the days
  $('<li></li>').html(i).attr('value', getProperDay(fDay+i-1)).appendTo('.calendar ul');
}

if(themonth>-1)
switch(tab) {
  case 'week':
    setupWeekActive(weekDay);
    break;
  case '2_weeks':
    setup2WeeksActive(weekDay);
    break;
}

//setup2WeeksActive('Sat');
//setupWeekActive('Sat');

function firstDay(month,year) {
  var t = new Date();
  t.setFullYear(year,month,1);
  var day = t.getDay();
  if(day==0) day=7;
  return day-1;
}

function numDays(month,year) {
  var t= new Date();
  t.setFullYear(year,month+1,0);
  return t.getDate();
}

function getProperDay(day) {
  return dayLabels[day%7];
}

function setup2WeeksActive(day) {
  $('.calendar li').each(function(){
    if(!($(this).hasClass('empty'))) {
      if($(this).attr('value')==day && weeks2Count>12){
        $(this).addClass('active');
        weeks2Count=0;
      }
      weeks2Count++;
    }
  });
}

function setupWeekActive(weekday) {
  $('.calendar li').each(function(){
    if(!($(this).hasClass('empty'))) {
      var dayVal = $(this).attr('value');
      if(dayVal==weekday && !checkEarly($(this).html())) {
        $(this).addClass('active');
      }
    }
  });
}

function setupMonthActive(weekday) {
  var count = 0;
  $('.calendar li').each(function(){
    if(!($(this).hasClass('empty'))) {
      if($(this).attr('value')==day && count>13 && !checkEarly($(this).html()) ){
        $(this).addClass('active');
        count=0;
      }
      count++;
    }
  });
}

function setActiveg(num){
  $('.calendar[value='+num+']').addClass('active');
}

function checkEarly(day) {
  return currentMonth==createdDate.getMonth() && themonth==0 && (day-createdDate.getDate())<3;
}

/*$('.calendar li').click(function(){ 
  $('.calendar li').removeClass('active');
  $(this).addClass('active');
});*/
}
});