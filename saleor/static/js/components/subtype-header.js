import {setActive} from './cart';

export default $(document).ready((e) => {

    /*let header_table = $('#cart_header_table');
    header_table.find('li').each(function() {
    $(this).click((e)=> {
      e.preventDefault();
      //setActive_table($(this).find('a'));
      //var tabVal = $(this).attr('value');
     // $tab = $(this).attr('value');
     // var countLines = filterCart(tabVal);
      //setSubTotal(tabVal);
     // setShippingTotal(tabVal);
     // checkEmptySub(countLines);
    })
  });*/

 // $('.order-details__addresses').addClass('d-none');
 let header = $('#cart_header_table');
 //initial
 let firstTab = header.find('li').first();
 //firstTab.removeClass('d-none');
 setActive(firstTab.find('a'), header);
 setupCart(firstTab.attr('value'));

  header.find('li').each( function() {
    $(this).click((e)=> {
      e.preventDefault();
      setActive($(this).find('a'), header);
      var tabVal = $(this).attr('value');
      setupCart(tabVal);
    })
  });

  function setupCart(val) {
    filterCart(val);
    setSubTotal(val);
    setShippingTotal(val);
  }

  function filterCart(val) {
    $('.order-details__product').each(function (){
      var countLines = 0;
      if($(this).find('#subtype_prop').attr('value') != val){
        $(this).addClass('d-none');
      }
      else {
        $(this).removeClass('d-none');
        countLines++;
      }
      return countLines;
    })
  }

  function setSubTotal(val) {
    $('#subtotals').find('p').addClass('d-none');
    $("p[id="+val+"_total]").removeClass('d-none');
  }

  function setShippingTotal(val) {
    var total = $('#ship_totals');
    total.find('p').addClass('d-none');
    total.removeClass('d-none');
    $("p[id=ship_"+val+"]").removeClass('d-none');
  }

});