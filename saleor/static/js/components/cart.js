import {getAjaxError} from './misc';

export const summaryLink = $('html').data('cart-summary-url');
export const $cartDropdown = $('.cart-dropdown');
export const $cartIcon = $('.cart__icon');
export const $addToCartError = $('.product__info__form-error small');
export const $removeProductSuccess = $('.remove-product-alert');
export const $addToCartSuccess = $('.add-product-alert');
export const $cantAddToCart = $('.cant-add-product-alert');
export const $emptySub = $('#empty_sub');
var $tab = 'all';
export const $cart = $('.cart');
export const onAddToCartError = (response) => {
  $addToCartError.html(getAjaxError(response));
  $cantAddToCart.removeClass('d-none');
};

export const onAddToCartSuccess = () => {
  $.get(summaryLink, (data) => {
    //$cartDropdown.html(data);
    $addToCartError.html('');
    $addToCartSuccess.removeClass('d-none');
    $(window).scrollTop(0);
    var newQunatity = $('.cart-dropdown__total').data('quantity');
    //$addToCartError.html(getAjaxError("Added to cart"));
    $('.badge').html(newQunatity).removeClass('empty');
    //$cartDropdown.addClass('show');
    //$cartIcon.addClass('hover');
   // $cartDropdown.find('.cart-dropdown__list').scrollTop($cartDropdown.find('.cart-dropdown__list')[0].scrollHeight);
    /*setTimeout((e) => {
      $cartDropdown.removeClass('show');
      $cartIcon.removeClass('hover');
    }, 2500);*/
  });
};

 export const setActive = (el, header)=> {
  removeAllActive(header, 'a');
  el.addClass('active');
}

  export const removeAllActive = (ext, inn)=>{
  ext.each(function() {
    $(this).find(inn).removeClass('active');
  });
}

export const checkEmptySub = (count, emptyView, holder) => {

  if(count==0){
    emptyView.removeClass('d-none');
    holder.addClass('d-none');
  }
  else {
    emptyView.addClass('d-none');
    holder.removeClass('d-none');
  }
}


export default $(document).ready((e) => {
  // Cart dropdown
  $.get(summaryLink, (data) => {
    $cartDropdown.html(data);
  });
  $('.navbar__brand__cart').hover((e) => {
    $cartDropdown.addClass('show');
    $cartIcon.addClass('hover');
  }, (e) => {
    $cartDropdown.removeClass('show');
    $cartIcon.removeClass('hover');
  });
  $('.product-form button').click((e) => {
    e.preventDefault();
    let quantity = $('#id_quantity').val();
    let variant = $('#id_variant').val();
    let subtype = $('input[name=subtype]:checked').val();
    $.ajax({
      url: $('.product-form').attr('action'),
      type: 'POST',
      data: {
        variant: variant,
        quantity: quantity,
        subtype: subtype
      },
      success: () => {
        onAddToCartSuccess();
      },
      error: (response) => {
        onAddToCartError(response);
      }
    });
  });

 


  let header_table = $('#cart_header_table');
  header_table.find('li').each(function() {
  $(this).click((e)=> {
    e.preventDefault();
    //setActive_table($(this).find('a'));
    //var tabVal = $(this).attr('value');
   // $tab = $(this).attr('value');
   // var countLines = filterCart(tabVal);
    //setSubTotal(tabVal);
   // setShippingTotal(tabVal);
   // checkEmptySub(countLines);
  })
});

  // product radio style 
 

 
  if (window.location.pathname.includes('/products/')) {
    setRadioBehav(); 
  }

  function setRadioBehav() {
   // $('variant_picker').find('label').addClass('btn btn-secondary variant-picker__option');
   let selectSubtype = $("#id_subtype");
   let label_id = $('input[name=subtype]:checked').attr('id');

   //$('.form-group').first().find('label').first().addClass('d-none');
   selectSubtype.find("label[for='"+label_id+"']").addClass('active');
   //$('.form-group label').addClass('radio-inline');

   //selectSubtype.find('div').removeClass('radio');
   //selectSubtype.find('div').addClass('radio-inline');

   //selectSubtype.addClass('btn-group');
   //selectSubtype.find("label").addClass('btn btn-secondary variant-picker__option');
   //selectSubtype.addClass('btn-group');
  // selectSubtype.attr('data-toggle', 'buttons');
  let radio = $("div.radio");
   radio.each(function(){
       $(this).click((e) => {
         removeAllActive(radio, 'label');
         $(this).find("label").addClass('active');
       });
   });
  }

  /*
  // cart filter
  let header = $('#cart_header');
  header.find('li').each(function() {
    $(this).click((e)=> {
      e.preventDefault();
      setActive($(this).find('a'));
      var tabVal = $(this).attr('value');
      filterCart(tabVal);
    })
  })

  function setActive(el){
    removeAllActive(header, 'a');
    el.addClass('active');
  }

  function filterCart(val) {
    $cartLine.each(function () {
      if(val=='all') {
        $(this).removeClass('d-none');
      }
      else {
        if($(this).find('input[name=subtype]:checked').val() != val){
          $(this).addClass('d-none');
        }
        else {
          $(this).removeClass('d-none');
        }
      }
     
    })
  }
*/



  // Cart quantity form

  let $cartLine = $('.cart__line');
  let $total = $('#all_total');
  let $week_total = $('#week_total');
  let $week2_total = $('#2_weeks_total');
  let $month_total = $('#month_total');
  let $month2_total = $('#2_month_total');

  let $cartBadge = $('.navbar__brand__cart .badge');
  let $closeMsg = $('.close-msg');

  $closeMsg.on('click', (e) => {
    $removeProductSuccess.addClass('d-none');
    $addToCartSuccess.addClass('d-none');
    $cantAddToCart.addClass('d-none');
  });
  $cartLine.each(function () {
    let $quantityInput = $(this).find('#id_quantity');
    let cartFormUrl = $(this).find('.form-cart').attr('action');
    let $qunatityError = $(this).find('.cart__line__quantity-error');
    let $subtotal = $(this).find('.cart-item-price p');
    let $deleteIcon = $(this).find('.cart-item-delete');
    $(this).on('change', $quantityInput, (e) => {
      let subtype = $(this).find('input[name=subtype]:checked').val();
      let newQuantity = $quantityInput.val();
      $.ajax({
        url: cartFormUrl,
        method: 'POST',
        data: {quantity: newQuantity, 
          subtype:subtype},
        success: (response) => {
          if (newQuantity === 0) {
            if (response.cart.numLines === 0) {
              $.cookie('alert', 'true', {path: '/cart'});
              location.reload();
            } else {
              $removeProductSuccess.removeClass('d-none');
              $(this).fadeOut();
            }
          } else {
            $subtotal.html(response.subtotal);
          }
          $cartBadge.html(response.cart.numItems);
          //$qunatityError.html('');
          $cartDropdown.load(summaryLink);
          deliveryAjax();
        },
        error: (response) => {
          $qunatityError.html(getAjaxError(response));
        }
      });
    });
    $deleteIcon.on('click', (e) => {
      let subtype = $(this).find('input[name=subtype]:checked').val();
      $.ajax({
        url: cartFormUrl,
        method: 'POST',
        data: {quantity: 0,
        subtype:subtype},
        success: (response) => {
          if (response.cart.numLines >= 1) {
            $(this).fadeOut();

           /* $total.html(response.totals.total);
            $week_total.html(response.totals.week_total);
            $week2_total.html(response.totals.week2_total);
            $month_total.html(response.totals.month_total);
            $month2_total.html(response.totals.month2_total);*/

            //$cartBadge.html(response.cart.numItems);
            $cartDropdown.load(summaryLink);
            $removeProductSuccess.removeClass('d-none');
          } else {
            $.cookie('alert', 'true', {path: '/cart'});
            location.reload();
          }
          deliveryAjax();
        }
      });
    });
  });

  
   // **cart filter**
  let header = $('#cart_header');
  header.find('li').each(function() {
    $(this).click((e)=> {
      e.preventDefault();
      setActive($(this).find('a'), header);
      var tabVal = $(this).attr('value');
      $tab = $(this).attr('value');
      var countLines = filterCart(tabVal);
      setSubTotal(tabVal);
      setShippingTotal(tabVal);
      checkEmptySub(countLines, $emptySub, $cart);
    })
  });

  

  
  function setProductSmallLabels(val, cart_line) {
    var sub_label = cart_line.find('#subtype_small');
    var var_label = cart_line.find('#variant_small');

    if(val=='all'){
      sub_label.removeClass('d-none');
      var_label.addClass('d-none');
    }
    else {
      sub_label.addClass('d-none');
      var_label.removeClass('d-none');
    }
  }

  function setSubTotal(val) {
    $('#subtotals').find('div').addClass('d-none');
    $("div[id="+val+"_total]").removeClass('d-none');
  }

  function setShippingTotal(val) {
    var total = $('#shipping_total');
    if(val=='all'){
      total.addClass('d-none');
    }
    else {
      total.find('.col-4 h3').addClass('d-none');
      total.removeClass('d-none');
      $("h3[id=ship_"+val+"]").removeClass('d-none');
    }
  }

 


  function filterCart(val) {
    
    var countLines = 0;
    $cartLine.each(function () {
      setProductSmallLabels(val, $(this));

      if(val=='all') {
        $(this).removeClass('d-none');
        countLines++;
      }
      else {
        if($(this).find('input[name=subtype]:checked').val() != val){
          $(this).addClass('d-none');
        }
        else {
          $(this).removeClass('d-none');
          countLines++;
        }
      }
    })
    return countLines;
  }

  



  // Delivery information

  let $deliveryForm = $('.deliveryform');
  let crsfToken = $deliveryForm.data('crsf');
  let countrySelect = '#id_country';
  let $cartSubtotal = $('.cart__subtotal');
  let deliveryAjax = (e) => {
    let newCountry = $(countrySelect).val();
    $.ajax({
      url: $('html').data('shipping-options-url'),
      type: 'POST',
      data: {
        'csrfmiddlewaretoken': crsfToken,
        'country': newCountry
      },
      success: (data) => {
        $cartSubtotal.html(data);
        setSubTotal($tab);
        setShippingTotal($tab);
      }
    });
  };

  $cartSubtotal.on('change', countrySelect, deliveryAjax);

  if ($.cookie('alert') === 'true') {
    $removeProductSuccess.removeClass('d-none');
    $.cookie('alert', 'false', {path: '/cart'});
  }
});
