import classNames from 'classnames';
import React, { Component, PropTypes } from 'react';

export default class SubTypeSelectionWidget extends Component {

  constructor(props) {
      super(props);
      this.state = {
          variants: [0, 1, 2, 3],
          active: 0,
          labels: ['week', '2 weeks', 'month', '2 month'],
          name: "Subtype"
      }
  }

  render() {
    //const { attribute, selected } = this.props;
    return (
      <div className="variant-picker">
        <div className="variant-picker__label">{this.state.name}</div>
        <div className="btn-group" data-toggle="buttons">
          {this.state.variants.map((i) => {
            const active = i === this.state.active;
            const labelClass = classNames({
              'btn btn-secondary variant-picker__option': true,
              'active': active
            });
            return (
              <label
                className={labelClass}
                key={i}
                onChange={() => { this.props.handleChange } }>
                <input
                  defaultChecked={active}
                  name={this.state.name}
                  type="radio"
                  value = {this.state.labels[i]}/>
                {this.state.labels[i]}
              </label>
            );
          })}
        </div>
      </div>
    );
  }

  
}
